const mongoose = require('mongoose');
const Bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema({
    email: String,
    password: String,
    name: String,
    roles: [String],
    is_active: Boolean,
});

userSchema.methods.comparePassword = function(plaintext, callback) {
    return callback(null, Bcrypt.compareSync(plaintext, this.password));
}

const User = mongoose.model('Employee', userSchema);

module.exports = User