const mongoose = require('mongoose');

const assert = require('assert');

const User = require('./schemas/user')


main().catch(err => console.log(err));

async function main() {
  await mongoose.connect(`mongodb://${process.env.username}:${process.env.password}@${process.env.host}/${process.env.database}`);
}


class Account {
  // This interface is required by oidc-provider
  static async findAccount(ctx, sub) {
    // @param ctx - koa request context
    // @param sub {string} - account identifier (subject)
    // @param token - is a reference to the token used for which a given account is being loaded,
    //   is undefined in scenarios where claims are returned from authorization endpoint

    // This would ideally be just a check whether the account is still in your storage
    console.log(sub)
    const account = await User.find({_id: mongoose.Types.ObjectId(sub)});
    console.log(account);

    if (!account) {
      return undefined;
    }

    return {
      accountId: sub,
      // and this claims() method would actually query to retrieve the account claims
      async claims() {
        // @param use {string} - can either be "id_token" or "userinfo", depending on
        //   where the specific claims are intended to be put in
        // @param scope {string} - the intended scope, while oidc-provider will mask
        //   claims depending on the scope automatically you might want to skip
        //   loading some claims from external resources or through db projection etc. based on this
        //   detail or not return them in ID Tokens but only UserInfo and so on
        // @param claims {object} - the part of the claims authorization parameter for either
        //   "id_token" or "userinfo" (depends on the "use" param)
        // @param rejected {Array[String]} - claim names that were rejected by the end-user, you might
        //   want to skip loading some claims from external resources or through db projection

        return {
          sub: sub,
          email: account.email,
          email_verified: account.email_verified,
        };
      },
    };
  }

  // This can be anything you need to authenticate a user
  static async authenticate(email, password) {
    try {
      assert(password, 'password must be provided');
      assert(email, 'email must be provided');
      const lowercased = String(email).toLowerCase();

      const account = await User.findOne({ username: lowercased }).exec();

      assert(account, 'invalid credentials provided');

      account.comparePassword(password, (error, match) => {
        assert(match)
      });

      console.log(account._id.valueOf())

      return account._id.valueOf();
    } catch (err) {
      return undefined;
    }
  }
}

module.exports = Account;